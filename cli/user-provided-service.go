package cli

import (
	"code.cloudfoundry.org/cli/plugin"
	"encoding/json"
	"errors"
	"fmt"
	. "gitlab.com/yaronp/portal-cli/utils"
	"time"
)

type UcsResponse struct {
	Metadata struct {
		GUID      string    `json:"guid"`
		URL       string    `json:"url"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
	} `json:"metadata"`

	Entity struct {
		Name               string        `json:"name"`
		Credentials        interface{}   `json:"credentials"`
		SpaceGUID          string        `json:"space_guid"`
		Type               string        `json:"type"`
		SyslogDrainURL     string        `json:"syslog_drain_url"`
		RouteServiceURL    string        `json:"route_service_url"`
		Tags               []interface{} `json:"tags"`
		SpaceURL           string        `json:"space_url"`
		ServiceBindingsURL string        `json:"service_bindings_url"`
		RoutesURL          string        `json:"routes_url"`
	} `json:"entity"`
}

func RunUPS(cliConnection plugin.CliConnection, args []string) error {

	if len(args) < 2 {
		fmt.Println("\nPlease provide service name")
		return errors.New("service name is missing")
	}

	service, err := cliConnection.GetService(args[1])
	if err != nil {
		fmt.Println("\nService does not exists")
		return errors.New("Service does not exists")
	}

	result, _ := CfExecAPI(cliConnection, "/v2/user_provided_service_instances/"+service.Guid)

	var a UcsResponse
	_ = json.Unmarshal(result, &a)

	if a.Entity.Type != "user_provided_service_instance" {
		fmt.Println("\nThis is not user provided service instance")
		return errors.New("This is not user provided service instance")
	}
	_ = PrettyPrint("Credentials:")
	_ = PrettyPrint(a.Entity.Credentials)
	_ = PrettyPrint("Tags:")
	_ = PrettyPrint(a.Entity.Tags)

	return nil
}
