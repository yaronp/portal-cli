package cli

import (
	"code.cloudfoundry.org/cli/cf/flags"
	"code.cloudfoundry.org/cli/plugin"
	"errors"
	"fmt"
	"gitlab.com/yaronp/portal-cli/token"
	"gitlab.com/yaronp/portal-cli/utils"
)



func RunCreateUserToken(cliConnection plugin.CliConnection, args []string) error {

	fc := flags.New()
	fc.NewBoolFlag("verbose", "v", "Verbose mode")
	fc.NewStringFlagWithDefault("app", "a", "application name", "portal-cf-dt-approuter")
	fc.NewStringFlag("user", "u", "user name")
	fc.NewStringFlag("password", "p", "password")
	fc.NewStringFlag("subdomain", "s", "sub domain")
	err := fc.Parse(args...)

	if err != nil {
		return nil
	}

	appName := fc.String("app")
	userName := fc.String("user")
	password := fc.String("password")
	subDomain := fc.String("subdomain")
	verbose := fc.Bool("verbose")

	if verbose {
		fmt.Println("Input Parameters:")
		fmt.Println("-------------------------------------")
		fmt.Println("appName:   ", fc.IsSet("appName"))
		fmt.Println("appName:   ", appName)
		fmt.Println("userName:  ", userName)
		fmt.Println("password:  ", password)
		fmt.Println("subDomain: ", subDomain)
	}
	if userName == "" || password == "" || subDomain == "" {
		fmt.Printf("Not enough parameters")
		return errors.New("Not Enough Parameters")
	}

	if verbose {
		fmt.Println("CfApplicationService ")
		fmt.Println(cliConnection.IsLoggedIn())
	}

	xsuaa := utils.CfApplicationService(cliConnection, appName, "xsuaa")
	if verbose {
		_ = utils.PrettyPrint(xsuaa)
	}

	xsuaaInstance := xsuaa[0]

	if verbose {
		_ = utils.PrettyPrint(xsuaaInstance)
	}

	clientId := xsuaaInstance.Credentials["clientid"].(string)
	clientSecret := xsuaaInstance.Credentials["clientsecret"].(string)
	uaaDomain := xsuaaInstance.Credentials["uaadomain"].(string)

	auth := token.UuaUserToken{}

	err = auth.Allocate(clientId, clientSecret, uaaDomain, subDomain, userName, password)
	if err == nil {
		fmt.Println(auth.IDToken)
		return nil
	}
	return err
}
