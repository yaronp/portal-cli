package cli

import (
	"code.cloudfoundry.org/cli/plugin"
	"fmt"
	"os/exec"
	"sync"
)

func worker(wg *sync.WaitGroup, cliConnection plugin.CliConnection, app string) {
	defer wg.Done()
	fmt.Println("restaging " + app)
	cmd := exec.Command("cf", "restage", app)
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("restaging " + app + "done" )
}

func RunAsyncRestage(cliConnection plugin.CliConnection, args []string) error {
	var wg sync.WaitGroup

	for i := 1; i < len(args); i++ {
		wg.Add(1)
		go worker(&wg, cliConnection, args[i])
	}
	wg.Wait()

	return nil
}
