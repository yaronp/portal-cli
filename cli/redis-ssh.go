package cli

import (
	"code.cloudfoundry.org/cli/cf/flags"
	"code.cloudfoundry.org/cli/plugin"
	"fmt"
	"gitlab.com/yaronp/portal-cli/utils"
)

func printNodeSshUsage() {
	fmt.Println("\nNAME:")
	fmt.Println("\tredis-ssh - Open tunnel to Redis using ssh")
	fmt.Println("USAGE:")
	fmt.Println("\tcf redis-ssh APP_NAME PORT [--verbose]")
	fmt.Println("EXAMPLE:")
	fmt.Println("\tcf redis-ssh portal-cf-assets 16200 --verbose")
	fmt.Println("OPTIONS:")
	fmt.Println("\t--verbose\t[Optional]Verbose mode")
	fmt.Println("")
}

func RunRedisSSH(cliConnection plugin.CliConnection, args []string) error {

	fc := flags.New()
	fc.NewBoolFlag("verbose", "v", "Verbose mode")
	err := fc.Parse(args...)

	if err != nil {
		return nil
	}
	verbose := fc.IsSet("verbose")

	if len(args) < 3 {
		fmt.Printf("Not enough arguments")
		printNodeSshUsage()
		return nil
	}

	appName := args[1]
	port := args[2]

	if verbose {
		fmt.Println("application name: ", appName)
		fmt.Println("port: ", port)
	}

	if verbose {
		fmt.Println("getting port of " + appName)
	}
	h, p := getRedisHostAndPort(cliConnection, appName, verbose)

	if verbose {
		fmt.Println("host " + h)
		fmt.Println("port " + p)
		fmt.Println("ssh -L " + port + ":" + h + ":" + p)
	}
	_, err = cliConnection.CliCommand("ssh", "-L", port+":"+h+":"+p, appName)
	return err
}

func getRedisHostAndPort(cliConnection plugin.CliConnection, app string, verbose bool) (host, port string) {
	redis := utils.CfApplicationService(cliConnection, app, "redis")
	credentials := redis[0].Credentials
	if verbose {
		fmt.Println("Redis Credentials:")
		_ = utils.PrettyPrint(credentials)
	}
	return credentials["host"].(string), credentials["port"].(string)
}
