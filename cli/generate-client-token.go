package cli

import (
	"code.cloudfoundry.org/cli/plugin"
	"flag"
	"fmt"
	token "gitlab.com/yaronp/portal-cli/token"
	"gitlab.com/yaronp/portal-cli/utils"
)

func RunCreateClientToken(cliConnection plugin.CliConnection, args []string) error {

	flagSet := flag.NewFlagSet("user-token", flag.ExitOnError)

	appName := flagSet.String("app", "portal-cf-dt-approuter", "application name")
	// config := flagSet.String("config", "", "port")
	// path := flagSet.String("path", "", "port")

	err := flagSet.Parse(args[1:])
	if err != nil {
		return err
	}

	xsuaa := utils.CfApplicationService(cliConnection, *appName, "xsuaa")
	xsuaaInstance := xsuaa[0]

	clientId := xsuaaInstance.Credentials["clientid"].(string)
	clientSecret := xsuaaInstance.Credentials["clientsecret"].(string)
	uaaUrl := xsuaaInstance.Credentials["url"].(string)

	auth := token.UuaClientToken{}

	err = auth.Allocate(clientId, clientSecret, uaaUrl)
	if err == nil {
		fmt.Println(auth.AccessToken)
		return nil
	}
	return err
}
