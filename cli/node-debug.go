package cli

import (
	"code.cloudfoundry.org/cli/cf/flags"
	"code.cloudfoundry.org/cli/plugin"
	"fmt"
)

func printUsage() {
	fmt.Println("\nNAME:")
	fmt.Println("\tnode-debug - Open tunnel for node.js debug using ssh")
	fmt.Println("USAGE:")
	fmt.Println("\tcf node-debug APP_NAME [--verbose]")
	fmt.Println("EXAMPLE:")
	fmt.Println("\tcf node-debug portal-cf-assets --verbose")
	fmt.Println("OPTIONS:")
	fmt.Println("\t--verbose\t[Optional]Verbose mode")
	fmt.Println("")
}

func RunNodeSsh(cliConnection plugin.CliConnection, args []string) error {

	fc := flags.New()
	fc.NewBoolFlag("verbose", "v", "Verbose mode")
	err := fc.Parse(args...)

	if err != nil {
		return nil
	}
	verbose := fc.IsSet("verbose")

	if len(args) < 2 {
		fmt.Printf("Not enough arguments")
		printUsage()
		return nil
	}

	appName := args[1]

	fmt.Println("1). Open chrome://inspect")
	fmt.Println("2). Open dedicated DevTools for Node")
	fmt.Println("3). Enjoy debugging while changing code live on cloudfoundry!")
	fmt.Println("")

	if verbose {
		fmt.Println("cf ssh -L " + appName + "port:localhost:9229")
	}

	// TODO: check that service exists
	// TODO: check that port is free

	_, err = cliConnection.CliCommand("ssh", "-L", "port:localhost:9229", appName)
	return err
}
