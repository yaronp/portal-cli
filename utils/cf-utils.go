package utils

import (
	"code.cloudfoundry.org/cli/plugin"
	"encoding/json"
	"fmt"
	cfEnv "github.com/cloudfoundry-community/go-cfenv"
	"github.com/mitchellh/mapstructure"
	"strings"
)

type CFError struct {
	Description string `json:"description"`
	ErrorCode   string `json:"error_code"`
	Code        int    `json:"code"`
}

func CfExecAPI(cliConnection plugin.CliConnection, args ...string) ([]byte, *CFError) {
	newArgs := []string{"curl"}
	newArgs = append(newArgs, args...)
	jsonResponseLines, err := cliConnection.CliCommandWithoutTerminalOutput(newArgs...)
	jsonResponse := strings.Join(jsonResponseLines, "")

	jsonResBytes := []byte(jsonResponse)
	var cfError CFError
	marshalErr := json.Unmarshal(jsonResBytes, &cfError)
	if marshalErr != nil {
		return jsonResBytes, &CFError{ErrorCode: "Go Error", Description: err.Error()}
	}
	if 0 < len(cfError.ErrorCode) {
		return jsonResBytes, &cfError
	}
	return jsonResBytes, nil
}

func CfAppEnv(cli plugin.CliConnection, app string) []byte {
	application, _ := cli.GetApp(app)
	url := "/v2/apps/" + application.Guid + "/env"
	response, _ := CfExecAPI(cli, url)
	return response
}

func cfAppService(cli plugin.CliConnection, systemEnvJson map[string]interface{}, service string) []cfEnv.Service {
	sysEnvServices := systemEnvJson["system_env_json"].(map[string]interface{})
	vcapServices := sysEnvServices["VCAP_SERVICES"].(map[string]interface{})
	services := make(map[string][]cfEnv.Service)
	for k, v := range vcapServices {
		var serviceInstances []cfEnv.Service
		mapstructure.WeakDecode(v, &serviceInstances)
		services[k] = serviceInstances
	}

	srv := make([]cfEnv.Service, len(services[service]))
	copy(srv, services[service])
	return srv
}

func CfApplicationService(cli plugin.CliConnection, app string, service string) []cfEnv.Service {
	appEnvVars := CfAppEnv(cli, app)
	var systemEnvJson map[string]interface{}
	json.Unmarshal(appEnvVars, &systemEnvJson)

	services := cfAppService(cli, systemEnvJson, service)
	return services
}

func PrettyPrint(v interface{}) (err error) {
	b, err := json.MarshalIndent(v, "", "  ")
	if err == nil {
		fmt.Println(string(b))
	}
	return
}
