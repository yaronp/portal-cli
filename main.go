// +build !V7

//  go build && cf install-plugin ./flp -f && cf redis-ssh -a portal-cf-assets -p 6379
// Build for windows:
//		GOOS=windows GOARCH=386 go build

package main

import (
	"code.cloudfoundry.org/cli/plugin"
	"gitlab.com/yaronp/portal-cli/cli"
)

type PortalPlugin struct {
}

func (c *PortalPlugin) Run(cliConnection plugin.CliConnection, args []string) {

	switch args[0] {
	case "redis-ssh":
		cli.RunRedisSSH(cliConnection, args)
		break
	case "node-debug":
		cli.RunNodeSsh(cliConnection, args)
		break
	case "user-token":
		cli.RunCreateUserToken(cliConnection, args)
		break
	case "client-token":
		cli.RunCreateClientToken(cliConnection, args)
		break
	case "async-restage":
		cli.RunAsyncRestage(cliConnection, args)
		break
	case "user-provided-service":
		cli.RunUPS(cliConnection, args)
		break
	}
}

func (c *PortalPlugin) GetMetadata() plugin.PluginMetadata {
	return plugin.PluginMetadata{
		Name: "SapPortalCLI",
		Version: plugin.VersionType{
			Major: 1,
			Minor: 0,
			Build: 0,
		},
		MinCliVersion: plugin.VersionType{
			Major: 6,
			Minor: 7,
			Build: 0,
		},
		Commands: []plugin.Command{
			{
				Name:     "redis-ssh",
				HelpText: "Open tunnel to Redis using ssh",
				Alias:    "rds",
				UsageDetails: plugin.Usage{
					Usage: "redis-ssh <app> <local-port> [--verbose]\n",
					Options: map[string]string{
						"verbose": "verbose mode",
					},
				},
			},
			{
				Name:     "node-debug",
				HelpText: "Open tunnel for remote node.js debug",
				Alias:    "ns",
				UsageDetails: plugin.Usage{
					Usage: "node-debug <app> [--vervose]",
					Options: map[string]string{
						"verbose": "verbose mode",
					},
				},
			},
			{
				Name:     "user-token",
				HelpText: "Generate user token",
				Alias:    "gut",
				UsageDetails: plugin.Usage{
					Usage: "user-token -user <user name> -password <password> -subdomain <sub domain> [-app <app>] [--verbose]\n",
					Options: map[string]string{
						"app":       "application name",
						"user":      "user name",
						"password":  "password",
						"subdomain": "consumer sub doamin",
						"verbose" : "verbose mode",
					},
				},
			},
			{
				Name:     "client-token",
				Alias:    "gct",
				HelpText: "Generate client token",
				UsageDetails: plugin.Usage{
					Usage: "client-token -app <app name> [-config <config>] [-path <path>]\n",
					Options: map[string]string{
						"app":    "application name [mandatory]",
						"config": "config json",
						"path":   "path",
					},
				},
			},
			{
				Name:     "async-restage",
				Alias:    "ar",
				HelpText: "Asynchronously re-stage applications",
				UsageDetails: plugin.Usage{
					Usage: "async-restage app1 [app2 ... appN]\n"},
			},
			{
				Name:     "user-provided-service",
				Alias:    "ups",
				HelpText: "view user provided service credentials",
				UsageDetails: plugin.Usage{
					Usage: "user-provided-service service\n"},
			},
			{
				Name:     "content-repo",
				HelpText: "Open tunnel to Redis using ssh",
				Alias:    "cr",
				UsageDetails: plugin.Usage{
					Usage: "content-repo [list] [--verbose]\n",
					Options: map[string]string{
						"verbose": "verbose mode",
					},
				},
			},

		},
	}
}

func main() {
	plugin.Start(new(PortalPlugin))
}
