# SAP Portal for CF - cf plugin 

## Build 

### MacOS

```GOOS=darwin GOARCH=amd64 go build -o portal```

### Windows 

```GOOS=windows GOARCH=amd64 go build -o portal.exe```

## Installation

```cf install-plugin ./portal -f``` 

**or**

```cf install-plugin portal.exe -f```

## Commands

### Create SSH tunnel for Redis 

```cf redis-ssh <application-name> <local-port>```

### Create SSH tunnel for node.js remote debugging

```cf node-debug <application-name>```

### Generate user token

```cf user-token -user <user name> -password <password> -subdomain <sub domain> [-app <app>]```

### Generate client token

```cf client-token -app <app name> [-config <config>] [-path <path>]```

### Asynchronously re-stage applications

```cf async-restage app1 [app2 ... appN]```

### View user provided service credentials

```cf ser-provided-service service```


