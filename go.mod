module gitlab.com/yaronp/portal-cli

go 1.13

require (
	code.cloudfoundry.org/cli v6.49.0+incompatible
	code.cloudfoundry.org/clock v1.0.0
	code.cloudfoundry.org/lager v2.0.0+incompatible
	code.cloudfoundry.org/trace-logger v0.0.0-20170119230301-107ef08a939d // indirect
	code.cloudfoundry.org/uaa-go-client v0.0.0-20190819190728-86bc743fdd89
	github.com/cloudfoundry-community/go-cfenv v1.18.0
	github.com/cloudfoundry-community/go-uaa v0.3.1
	github.com/cloudfoundry-incubator/uaa-go-client v0.0.0-20190819190728-86bc743fdd89
	github.com/cloudfoundry/log-stream-cli v0.5.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
