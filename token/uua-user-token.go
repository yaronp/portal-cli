package token

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type UuaUserToken struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	IDToken      string `json:"id_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
	Scope        string `json:"scope"`
	Jti          string `json:"jti"`
}

func (a *UuaUserToken) Allocate(clientId, clientSecret, uaaDomain, subDomain, userName, password string) error {

	url := "https://" + subDomain + "." + uaaDomain + "/oauth/token"

	method := "POST"
	payload := strings.NewReader("grant_type=password&username=" + userName + "&password=" + password)
	client := &http.Client{}

	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return err
	}

	basicAuthDecoded := clientId + ":" + clientSecret
	basicAuthBase64 := base64.StdEncoding.EncodeToString([]byte(basicAuthDecoded))

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", "Basic "+basicAuthBase64)

	res, err := client.Do(req)

	if err != nil {
		fmt.Printf(err.Error())
		return err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	err = json.Unmarshal(body, a)
	return err
}
