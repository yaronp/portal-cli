package token

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type UuaClientToken struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	Scope       string `json:"scope"`
	Jti         string `json:"jti"`
}

func (a *UuaClientToken) Allocate(clientId, clientSecret, uaaUrl string) error {

	url := uaaUrl + "/oauth/token"

	method := "POST"
	payload := strings.NewReader("grant_type=client_credentials&username=" + clientId + "&password=" + clientSecret)
	client := &http.Client{}

	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return err
	}

	basicAuthDecoded := clientId + ":" + clientSecret
	basicAuthBase64 := base64.StdEncoding.EncodeToString([]byte(basicAuthDecoded))

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", "Basic "+basicAuthBase64)

	res, err := client.Do(req)

	if err != nil {
		fmt.Printf(err.Error())
		return err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	err = json.Unmarshal(body, a)
	return err
}
